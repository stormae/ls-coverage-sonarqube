#### Deploying SonarQube Locally via Docker
```
docker-compose -f sonarqube-docker-compose.yml up -d
```

#### Running SonarQube Analysis with Coverage
```
sh sonarqube.sh
```