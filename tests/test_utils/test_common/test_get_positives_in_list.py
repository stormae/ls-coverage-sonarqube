from utils.common import get_positives_in_list

import unittest


class TestGetPositives(unittest.TestCase):

    def test_empty(self):
        self.assertEqual([], get_positives_in_list([]))
