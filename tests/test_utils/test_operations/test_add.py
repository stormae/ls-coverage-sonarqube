from utils.operations import add

import unittest


class TestAdd(unittest.TestCase):

    def test_success(self):
        self.assertEqual(3, add(1, 2))
