from utils.operations import divide

import unittest


class TestDivide(unittest.TestCase):

    def test_divide_by_zero(self):
        self.assertEqual(None, divide(1, 0))

    def test_safe(self):
        self.assertEqual(0.5, divide(1, 2))
