def foo():
    print("I'm in Foo!")


def bar():
    print("I'm in Bar!")


def print_name(name):
    if isinstance(name, str):
        print(name)


foo()
print_name("Mae")

# Uncomment this to solve missing coverage on cov_demo.py
# bar()

# Uncomment this to solve partial coverage on cov_demo.py
# print_name(None)
