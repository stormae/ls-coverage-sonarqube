coverage run \
  --source=src/ \
  --branch -m unittest discover \
&& coverage xml \
&& sonar-scanner \
  -Dsonar.projectKey=project_key_here \
  -Dsonar.sources=src/ \
  -Dsonar.host.url=http://localhost:8080 \
  -Dsonar.login=token_here \
  -Dsonar.python.coverage.reportPaths=coverage.xml \
  -Dsonar.projectVersion=version_here \
  -Dsonar.scm.disabled=True