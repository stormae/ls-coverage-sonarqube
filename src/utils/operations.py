def add(x, y):
    # Uncomment this to demo sonarqube unused variable code smell
    # a = 0

    return x + y


def divide(x, y):
    if y == 0:
        print("Cannot divide by zero!")
        return None

    return x / y


# Uncomment this to demo sonarqube new code coverage policy
# def minus(x, y):
#     print("This")
#     print("is")
#     print("a")
#     print("new")
#     print("block")
#     print("of")
#     print("code")
#     print("that")
#     print("has")
#     print("no")
#     print("coverage")
#     print("This")
#     print("is")
#     print("another")
#     print("new")
#     print("block")
#     print("of")
#     print("code")
#     print("that")
#     print("has")
#     print("no")
#     print("coverage")
#     print("Last")
#     print("new")
#     print("block")
#     print("of")
#     print("code")
#     print("that")
#     print("has")
#     print("no")
#     print("coverage")
#     return x - y
